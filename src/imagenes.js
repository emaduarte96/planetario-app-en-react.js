import logo from './imag/logo.png';
import cienciaU from './imag/cienciaU.jpg';
import fisicoQ from './imag/fisicoQ.jpg';
import introA from './imag/introA.jpg';
import listado from './imag/listado.jpg';
import mapaC from './imag/mapaC.jpg';
import registrar from './imag/registrar.jpg';


export default {

    "img1": logo,

    "img2": registrar,

    "img3": listado,

    "img4": mapaC,

    "img5": cienciaU,

    "img6": fisicoQ,

    "img7": introA,

}