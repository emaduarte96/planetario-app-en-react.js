import './css/App.css';
import './css/normal.css';
import './css/footer.css';
import './css/listado.css';
import Header from './componet/header'
import Botones from './componet/card';
import Dash from './componet/dash'
import Footer from './componet/footer'
import Buscador from './componet/buscador';
import MapaDelCielo from './componet/mapaCielo';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";



function App() {

    return (
      
        <div className="App">
          <div id="cuerpo">
            <Header/>
            <Router>
              <Botones/>
            
              <div id="contenedor" className = "container wow zoomIn">
                <Switch>
                  <Route path="/dash">
                    <Dash/>
                  </Route>
                  <Route path="/buscador">
                    <Buscador/>
                  </Route>
                  <Route path="/mapaCielo">
                    <MapaDelCielo/>
                  </Route>
                  <Route path="/">
                    <Dash/>
                  </Route>
          
                </Switch>
          
                
              </div> 
            </Router> 
            <Footer/>
            
          </div>
        </div>
      
    
    );
}

export default App;
