import React from 'react'

export default function Footer(){

    return(
        <div id="infoFooter">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
              <path fill="#020e42" fillOpacity="1" d="M0,256L34.3,234.7C68.6,213,137,171,206,138.7C274.3,107,343,85,411,90.7C480,96,549,128,617,149.3C685.7,171,754,181,823,176C891.4,171,960,149,1029,154.7C1097.1,160,1166,192,1234,192C1302.9,192,1371,160,1406,144L1440,128L1440,320L1405.7,320C1371.4,320,1303,320,1234,320C1165.7,320,1097,320,1029,320C960,320,891,320,823,320C754.3,320,686,320,617,320C548.6,320,480,320,411,320C342.9,320,274,320,206,320C137.1,320,69,320,34,320L0,320Z"></path>
          </svg>
          
          <div className="copiYbtn">
              <div>
                  <button className="btn btn-large btn-danger">Cerrar Sesión</button>
              </div>
              <div>
                  <p>Copyright © 2021 || Asociación Amigos del Observatorio Astronómico y Planetario Municipal de Rosario | Todos los derechos reservados. </p>
              </div>
              
          </div>
      
        </div>
    )
}
