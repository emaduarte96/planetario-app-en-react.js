import React from 'react'
import TarjetaDash from './tarjetas'


export default function Dash(){

    return(
        <div className="row">
        <div id="dash" className="p-0 m-0 col-12 col-md-4">
                <div className="padre p-0 m-0">
                    <div className="hijo p-0 m-0 sombra">
                        <div className="titulo">
                            <h2>Nuevo Socio</h2>
                        </div>
                        <form className ="form" id="formularioDash">
                                <label>Nombre y Apellido</label>
                                <input type="text" name='nombre' id="nombre" ></input>
                                <label>Ciudad</label>
                                <input type="text" name='ciudad' id='ciudad'></input>
                                <label>Dirección</label>
                                <input type="text" name='direccion' id='direccion'></input>
                                <label>Fecha de Nacimiento</label>
                                <input type="text" name='fechaNacimiento' id='fechaNacimiento'></input> 
                                <label>Numero de documento</label>
                                <input type="text" name='dni' id='dni'></input>
                                <label>Email</label>
                                <input type="text" name='email' id='email'></input>
                                <label>Numero de telefono</label>
                                <input type="text" name='telefono' id='telefono'></input>
                                <label>Ocupacion</label>
                                <input type="text" name='ocupacion' id='ocupacion'></input>

                                <label className ="mt-4">Estatus</label>
                                

                                <div id="checks">
                                    <input type="radio" name="estatus" value="Socio"></input>
                                    <label>Socio</label>
                                  
                                    <input type="radio" name="estatus" value="No Socio"></input>
                                    <label>No Socio</label>
                                </div>
                              

                                <label className ="mt-4">Curso</label>

                                <div id="checks" className="mt-4">
                                    <input type="checkbox" value="MC" name="mapaCielo"></input>
                                    <label>MC</label>
                                  
                                    <input type="checkbox" value="CU" name="cienciaUni"></input>
                                    <label>CU</label>

                                    <input type="checkbox" value="FQ" name="fisicoQuimica"></input>
                                    <label>FQ</label>

                                    <input type="checkbox" value= "IA" name="intro"></input>
                                    <label>IA</label>
                                  </div>

                                  <div  className ="boton mt-5">
                                      <button type="submit" className ="btn btn-large btnSubmit" id="btnSubmit">Agregar socio</button>
                                  </div>
                        </form>
                    
                </div>
            </div>
        </div>
        <div id="cajaPrincipal" className ="col-12 col-md-8 p-5 mt-0">
            <div id="busquedaForm" className="sombra">
                <h3>Buscador</h3>
                <form>
                    <input type="search" className = 'm-2' name='busqueda' id='busqueda' placeholder="Buscar..."></input>
                    <button className ="btn">Buscar</button>
                </form>
            </div>
            <div id = "cajaPrincipalDos" className ="p-0 mt-4">

                <TarjetaDash/>

            </div>
        </div>
    </div>
    )
}