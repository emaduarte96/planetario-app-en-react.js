import React from 'react';
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card'
import { useEffect, useState } from 'react';

function Buscador(){
    const url = 'http://rubiconrosario.com/Api/pintarListas.php';
    const [todos, setTodos] = useState([]);
    const FetchApi = async () => {
    const response = await fetch(url);
    const responseJSON = await response.json();
    setTodos(responseJSON);
    }
    const [totalSocios, setSocios] = useState(0)
    const totalInscriptos = () =>{
        setSocios(todos.length);
    }

    useEffect(() => {
    FetchApi();
    }, []);

    useEffect(() => {
        totalInscriptos();
      });

    return(
        <div id="contenedorFooter" className = "mt-5">
                <div className="footer">
                    <div className="search">
                        <input type="search" name="Buscar" id="search" placeholder="Buscar..."></input>
                        <button className="btn btn-large btn-dark">Buscar</button>
                    </div>
                    
                    <div className="btns">
                        <button className="btn btn-large btn-danger">Lista PDF</button>
                        <button className="btn btn-large btn-danger">Carnets PDF</button>
                        <button className="btn btn-large btn-success">Exportar Excel</button>
                    </div>
                    <div className="contadorTotal">
                        <h5>Inscriptos: {totalSocios}</h5>
                    </div>

                </div>
           
                <div id="contenedorLista" className="p-0 m-0">
                    <div className="contenedorCards p-0 m-0">
                        <ul className="m-0 p-0">
                            {!todos ? 'No hay socios registrados' :
                            todos.map(
                                socio =>{
                                    const {nombre, estatus,dni , ciudad, direccion, fechaNacimiento, email, telefono, ocupacion, MC, CU, FQ, IA} = socio;
                                    
                                    return  <li>
                                                <Accordion id="tarjetaLista">
                                                    <Card>
                                                        <Accordion.Toggle as={Card}   variant="link" eventKey="0">
                                                            <div>
                                                                <div className="card-header" id="headingOne">
                                                                    <div className="datosPrincipales">
                                                                        <div>
                                                                            <h6>{nombre}</h6>
                                                                        </div>
                                                                        <div>
                                                                            <h6>{dni}</h6>
                                                                        </div>
                                                                        <div>
                                                                            <h6>{estatus}</h6>
                                                                        </div>
                                                                        <div>
                                                                            <h6>{MC} {CU} {FQ} {IA}</h6>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                
                                                                </div>
                                                            </div>
                                                        </Accordion.Toggle>
                                                        <Accordion.Collapse eventKey="0">
                                                            
                                                                <div className="card-body">
                                                                    <div id="tarjeta">
                                                                            <div className="info">
                                                                                <div className="datos">

                                                                                    <div>
                                                                                        <h6>Ciudad:</h6>
                                                                                    </div>
                                                                                    <div>
                                                                                        <h6>Dirección:</h6>
                                                                                    </div>
                                                                                    <div>
                                                                                        <h6>Fecha de nacimiento:</h6>
                                                                                    </div>
                                                                            
                                                                                </div>
                                                                        
                                                                                <div className="datos">

                                                                                    <div>
                                                                                        <p>{ciudad}</p>
                                                                                    </div>
                                                                                    <div>
                                                                                        <p>{direccion}</p>
                                                                                    </div>
                                                                                    <div>
                                                                                        <p>{fechaNacimiento}</p>
                                                                                    </div>
                                                                                </div>
                                                                                    
                                                                            </div>
                                                                        
                                                                        
                                                                        <div className="info">
                                                                            <div className="datos">
                                                                                <div>
                                                                                    <h6>E-mail:</h6>
                                                                                </div>
                                                                                <div>
                                                                                    <h6>Teléfono:</h6>
                                                                                </div>
                                                                                <div>
                                                                                    <h6>Ocupación:</h6>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div className="datos">
                                                                                <div>
                                                                                    <p>{email}</p>
                                                                                </div>
                                                                                <div>
                                                                                    <p>{telefono}</p>
                                                                                </div>
                                                                                <div>
                                                                                    <p>{ocupacion}</p>
                                                                                </div>
                                                                                
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                            
                                                            
                                                        </Accordion.Collapse>
                                                    </Card>
                                                </Accordion>
                                            
                                            </li>
                                        })
                                    }
                        </ul>
                    </div>
                </div>
        </div> 
        
    )
}

export default Buscador;