import React from 'react'
import { useEffect, useState } from 'react';

export default function TarjetaDash(){
    const url = 'http://rubiconrosario.com/Api/pintarListas.php';
    const [todos, setTodos] = useState();
    const FetchApi = async () => {
    const response = await fetch(url);
    const responseJSON = await response.json();
    setTodos(responseJSON);
    }
    useEffect(() => {
    FetchApi();
    }, []);

    return(
        <ul id="lista" className="p-0 m-0">
            {!todos ? 'No hay socios registrados' :
            todos.map(
                socio =>{
                    const {nombre, estatus} = socio
                    return <li>
                                <div className ="card">
                                    <div id = "info">
                                        <div className ="dato p-0">
                                            <p>{nombre}</p>
                                        </div>
                                        <div className ="dato">
                                            <strong>
                                                <p>{estatus}</p>
                                            </strong>
                                            
                                        </div>
                                    </div>
                                    
                                    <div id = "botones">
                                        <button className="btn btn-large btn-primary">Editar</button>
                                        <button className="btn btn-large btn-danger">Eliminar</button>
                                        <button className="btn btn-large btn-success">Imprimir PDF</button>
                                    </div>
                                    
                                </div>
                            </li>
                })
            }
        </ul>
        
    )
}