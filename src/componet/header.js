import React from 'react'
import imagenes from '../imagenes'


export default function Header(){

    return(
        <div className="texto p-0 m-0">
          <div id="svg">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
              <path fill="#020e42" fillOpacity ="1" d="M0,128L26.7,133.3C53.3,139,107,149,160,176C213.3,203,267,245,320,266.7C373.3,288,427,288,480,288C533.3,288,587,288,640,293.3C693.3,299,747,309,800,293.3C853.3,277,907,235,960,202.7C1013.3,171,1067,149,1120,170.7C1173.3,192,1227,256,1280,282.7C1333.3,309,1387,299,1413,293.3L1440,288L1440,0L1413.3,0C1386.7,0,1333,0,1280,0C1226.7,0,1173,0,1120,0C1066.7,0,1013,0,960,0C906.7,0,853,0,800,0C746.7,0,693,0,640,0C586.7,0,533,0,480,0C426.7,0,373,0,320,0C266.7,0,213,0,160,0C106.7,0,53,0,27,0L0,0Z"></path>
          </svg>
          </div>
          <div className ="container">
              <div id="head" className ="row">
                <div className = "logo col-4 col-md-4">  
                    <div><img src={imagenes.img1}/></div>
                </div>
                  
                <div className = "titulo col-8 col-md-8"> 
                    <div><h1>Asociación Amigos del Observatorio Astronómico y Planetario Municipal de Rosario</h1></div>
                </div>
              </div>
          </div>
        </div>
    )
}