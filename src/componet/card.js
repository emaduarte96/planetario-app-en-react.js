import React from 'react'
import {Link} from "react-router-dom";
import imagenes from '../imagenes'
const {img2, img3, img4, img5, img6, img7} = imagenes;
const data = [{img:img2, id: 'registrar', path: '/dash'}, {img: img3, id: 'listado', path: '/buscador'}, {img: img4, id: 'mapaC', path: '/mapaCielo'}, {img: img5, id: 'cienciaU'}, {img: img6, id:'fisicoQ'}, {img: img7, id: 'introA'}];


export default function Botones(){
    
    return( 
            
            <div id="seccionesBtn">
                {data.map(({id, img, path}) =>
                    <Link to={path}><div id = {id}><img src={img} className="sombra"></img></div></Link>
                )}
            </div>
        
    )
}